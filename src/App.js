import React from 'react';
import './App.css';
import MainComponent from './MainComponent/MainComponent';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MainComponent />
      </header>
    </div>
  );
}

export default App;
