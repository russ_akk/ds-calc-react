import React, { useEffect, useState } from 'react'
import styles from './MainComponent.module.css'
import ReactFancyBox from 'react-fancybox'
import 'react-fancybox/lib/fancybox.css'
import SliderComponent from './SliderComponent'

export default function MainComponent() {
    let filterType = "i";
    const [slideItems, setSliderItems] = useState([
        { id: 1, value: 1, price: 0, text: "Quality<br /> (settings of materials, light)", lowText: "low", mediumText: "medium", highText: "high"},
        { id: 2, value: 1, price: 0, text: "Detailing<br /> (small objects, decor elements)", lowText: "low", mediumText: "medium", highText: "high"},
        { id: 3, value: 1, price: 0, text: "3D Scene Scale", lowText: "low", mediumText: "medium", highText: "high"},
    ]);
    const [forPingUseEffect, setForPingUseEffect] = useState(false);
    const [dyh3DScene, set3DScene] = useState(1);
    const [makeFromPhoto, setMakeFromPhoto] = useState(1);
    const [imageName, setImageName] = useState("images/i111.jpg");
    const [imagesCount, setImagesCount] = useState(1);
    const [roundOfFixes, setRoundOfFixes] = useState(3);
    const [price, setPrice] = useState(50);
    const [makeFromPhotoDisabled, setMakeFromPhotoDisabled] = useState(true);
    const [makeFromPhotoChecked, setMakeFromPhotoChecked] = useState(2);
    const onChange3dsceneHandler = (e) => {
        switch (e.target.value) {
            case "1":
                setMakeFromPhoto(1);
                setMakeFromPhotoChecked(2);
                setMakeFromPhotoDisabled(false);
                break;
            default:
                setMakeFromPhoto(1);
                setMakeFromPhotoDisabled(true);
            }
        }
    const onChangeMakeFromPhotoHandler = (e) => {
        switch (e.target.value) {
            case "0":
                setMakeFromPhoto(2);
                setMakeFromPhotoChecked(0);
                break;
            case "1":
                setMakeFromPhoto(1.3);
                setMakeFromPhotoChecked(1);
                break;
            default:
                setMakeFromPhoto(1);
                setMakeFromPhotoChecked(2);
            }
        }
    const onChangeImagesCountHandler = (e) => {
        if (e.target.value >= 1) {
            setImagesCount(e.target.value)
        } else {
            setImagesCount(1)
        }
    }
    const onChangeRoundOfFixesHandler = (e) => {
        if (e.target.value >= 3) {
            setRoundOfFixes(e.target.value)
        } else {
            setRoundOfFixes(3)
        }
    }
    const handleChangeSlider = (id, value) => {
        let sliderPrice = (value - 1) * 30;
        slideItems.forEach(item => {
            if (item.id === id) {
                item.value = value;
                item.price = sliderPrice;
            }
        });
        setSliderItems(slideItems);
        setForPingUseEffect(!forPingUseEffect);
    }
    useEffect(() => {
        let newPrice = 50;
        let newImageName = "images/" + filterType;
        slideItems.forEach(item => {
            newPrice += item.price;
            newImageName += item.value;
        });
        newPrice *= makeFromPhoto;
        newPrice = (newPrice * dyh3DScene * imagesCount * (1 + (roundOfFixes - 3) * 0.1)).toFixed(1);
        setPrice(newPrice);

        newImageName += ".jpg";
        setImageName(newImageName);
    }, [forPingUseEffect, slideItems, dyh3DScene, filterType, imagesCount, makeFromPhoto, roundOfFixes])

    return (
        <div className={styles.mainSection}>
            <h1>Calculator</h1>
            <div className={styles.filterClass}>
                <div className={styles.flexMainClass}>
                    <div className={styles.leftSideClass}>
                        {slideItems.map(slideItem => {
                            return <SliderComponent slider={slideItem} changeSliderFunction={handleChangeSlider} key={slideItem.id} />
                        })}
                    </div>
                    <div className={styles.rightSideClass}>
                        <div className={styles.marginBottomClass}>
                            <div className={styles.inputTextClass}>
                                Do you have 3D scene?
                            </div>
                            <div className={styles.radioClass} onChange={onChange3dsceneHandler}>
                                <input type="radio" name="3dscene" id="3dscene1" defaultChecked value="0" />
                                <label className={styles.labelClass} htmlFor="3dscene1">Yes, I have.</label>
                                <br />
                                <input type="radio" name="3dscene" id="3dscene2" value="1" />
                                <label className={styles.labelClass} htmlFor="3dscene2">No, I haven't. Please, do it for me.</label>
                            </div>
                        </div>
                        <div className={styles.marginBottomClass}>
                            <div className={styles.inputTextClass}>
                                Make a 3D Scene from photo or drawing?
                            </div>
                            <div className={styles.radioClass} onChange={onChangeMakeFromPhotoHandler}>
                                <input type="radio" name="makeFromPhoto" id="makeFromPhoto1" value="0" disabled={makeFromPhotoDisabled}
                                    checked={makeFromPhotoChecked === 0} /> 
                                <label className={styles.labelClass} htmlFor="makeFromPhoto1">Exact copy.</label>
                                <br />
                                <input type="radio" name="makeFromPhoto" id="makeFromPhoto2" value="1" disabled={makeFromPhotoDisabled}
                                    checked={makeFromPhotoChecked === 1} />
                                <label className={styles.labelClass} htmlFor="makeFromPhoto2">Only walls, windows, doors. Make furniture similar.</label>
                                <br />
                                <input type="radio" name="makeFromPhoto" id="makeFromPhoto3" value="2" disabled={makeFromPhotoDisabled}
                                    checked={makeFromPhotoChecked === 2} />
                                <label className={styles.labelClass} htmlFor="makeFromPhoto3">Do what you want.</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.flexClass}>
                    <div className={styles.width40Class}>
                        <div className={styles.inputTextClass} style={{marginLeft: 40 + 'px'}}>
                            Number of pictures:
                        </div>
                        <div className={styles.inputClass}>
                            <input type="number" name="imagesCount" defaultValue="1" min="1" max="100" onChange={onChangeImagesCountHandler} />
                        </div>
                    </div>
                    <div className={styles.width40Class}>
                        <div className={styles.inputTextClass}>
                            Round of fixes:<br />
                            <span style={{fontSize: 0.8 + 'em'}}>*the first three are free</span>
                        </div>
                        <div className={styles.inputClass}>
                            <input type="number" name="roundOfFixes" defaultValue="3" min="3" max="100" onChange={onChangeRoundOfFixesHandler} />
                        </div>
                    </div>
                </div>
                <div className={styles.flexClass}>
                    <div className={styles.width40Class}>
                        <div className={styles.inputTextClass} style={{marginLeft: 40 + 'px', marginTop: 20 + 'px', fontSize: 0.8 + 'em'}}>
                            Price: {price}$
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.imagesSection}>
                <ReactFancyBox
                    defaultThumbnailWidth={1400}
                    defaultThumbnailHeight={683}
                    image={imageName}/>
            </div>
        </div>
    )
}