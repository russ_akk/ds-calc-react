import React from 'react'
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import styles from './MainComponent.module.css'

export default function SliderComponent ({slider, changeSliderFunction}) {
    return (
        <div className={styles.flexClass}>
            <div className={styles.sliderTextClass} dangerouslySetInnerHTML={{__html: slider.text}} />
            <div className={styles.sliderClass}>
                <Slider 
                    min={1}
                    max={3}
                    marks={{
                        1:{
                            label: slider.lowText
                        },
                        2:{
                            label: slider.mediumText
                        },
                        3:{
                            label: slider.highText
                        },
                    }}
                    onChange={value => {changeSliderFunction(slider.id, value)}}
                    />
            </div>
        </div>
    );
}
